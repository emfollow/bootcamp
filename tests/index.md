# Testing Code (14:00-15:30, Piotrzkowski)
Homework: get a working Python 3.6 environment set up on your machine with following packages:
- pytest
- unittest
- ligo-gracedb
- ipython (optional)

If you haven't already, clone the bootcamp git repository using:
```
git clone https://git.ligo.org/emfollow/bootcamp
```

## Unit Tests
For code to be trusted and used professionally it must be thoroughly tested. This is especially true for LIGO production code where bugs can lead to code failing/manual intervention when we detect events, and at worst, loss of science due to delayed emfollow-up. In order to release code quickly and with minimal bugs we use automated testing, often called unittests, that can be launched locally and can be set to run everytime a merge request is submitted/changed/merged. **Note:** often repositories are set up so that code changes *can't* be merged to the master branch unless they pass all tests. 

In this tutorial we will:
- Create basic unittests
- Learn how to run these tests
- Incorporate patching and parameterization
- Create a Mock version of GraceDB to test with
- Time permitting, learn to run tests automatically in GitLab

### Create functions to test

Before we can create any sort of test we first need to create some functions to make a test for. Let's create a new file in the `bootcamp/tests` directory called `functions.py` (use `vim functions.py` or any other method you want). Let us define a few basic functions in this file:
```
def _add(a,b):
    return a + b
    
def _multiply(a,b):
    return a * b
    
def _dotproduct(a,b):
    if len(a)==len(b):
        sum = 0
        for i in range(len(a)):
            sum = _add(sum, _multiply(a[i], b[i]))
        return sum
    else:
        raise AssertionError('Arrays must be equal length')
```
Feel free to add any additional functions at this time as well.

Let's quickly check that these tests are working. We can open an ipython terminal using the terminal command `ipython` in the same directory and type the following:
```
In [1]: import functions                                                                                                                                                                                                                                                                                                                                                    

In [2]: functions._add(2,2)                                                                                                                                                                                                                                                                                                                                                 
Out[2]: 4

In [3]: functions._multiply(2,3)                                                                                                                                                                                                                                                                                                                                            
Out[3]: 6

In [4]: functions._dotproduct([1,1],[1,1])                                                                                                                                                                                                                                                                                                                                  
Out[4]: 2
```
Since the outputs look good we can proceed to the automatic tests.

### Create a basic test

Now let's starting making some basic unittests. In the same directory let's create another file called `test_functions.py` (**note** that the filename should be of this format for the tests to be found by `pytest`). In the header of this file we need to import both the functions we wish to test and any python testing packages. We will be using the following:
```
import functions
import pytest
from unittest.mock import call, patch
from math import isclose
```

Next let us define the test functions. Typically we want at least one test function per function we wish to test. We want to design our tests such that if they fail an error is raised (otherwise the test will be assumed to be passed). For `_add` let us use the following:

```
def test_add():
    assert functions._add(2,2)==4
```
Here if `_add` works correctly and calculates `2+2=4` like it should, the test will pass. Otherwise an `AssertionError` will be raised and the test will fail. We will work on tests for the other two functions in the later sections.

**Note:** we typically don't place functions and their tests in the same directory. See [gwbroccoli](https://git.ligo.org/brandon.piotrzkowski/gwbroccoli) for a better example of how we usually arrange directories.

### Run tests

In this case we could run the tests 'manually' by executing the function in `test_functions.py` in an ipython terminal but this can get tedious fast if we have many functions and many test scripts. So instead we can run all the tests we want using pytest. To do this we to make a `setup.py` and `setup.cfg` file in the main directory (already done here but worth following along) with the following contents:

`setup.py`:
```
from setuptools import setup

setup(
    name='emfollow-bootcamp',
    version='0.1.0',
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    install_requires=['ligo-gracedb>=2.2.0']
)

```
where we are giving some metadata information, pointing to pytest when we call `setup.py` to run tests and

`setup.cfg`:
```
[aliases]
test=pytest
```
where again, we point to pytest when we call for the tests to be executed.

We can now run all the tests when we use the terminal command:
```console
brandonpiotrzkowski$ python setup.py test
```
We should see an output like the following
```console
running pytest
running egg_info
writing UNKNOWN.egg-info/PKG-INFO
writing dependency_links to UNKNOWN.egg-info/dependency_links.txt
writing top-level names to UNKNOWN.egg-info/top_level.txt
reading manifest file 'UNKNOWN.egg-info/SOURCES.txt'
writing manifest file 'UNKNOWN.egg-info/SOURCES.txt'
running build_ext
==================================== test session starts =====================================
platform darwin -- Python 3.7.3, pytest-4.5.0, py-1.8.0, pluggy-0.11.0
rootdir: /Users/brandonpiotrzkowski/ligogit/forks/bootcamp
plugins: celery-4.3.0
collected 1 item                                                                             

tests/test_functions.py .                                                              [100%]

================================== 1 passed in 0.09 seconds ==================================

```
where the dots after the test names indicate that the test passed (x's indicate failure.)

**Note:**
It is good practice to 'mess around' with your unittests to make sure they are working correctly and not passing for the wrong reason. I find it helpful to write one of the tests incorrectly on purpose to make sure the test fails when you expect it to.


### Using parameterized tests

Perhaps we don't want to run a test for just one example but many examples. For this we can use `@pytest.mark.parametrize`. Consider the following example for `_mutliply`

```
@pytest.mark.parametrize(
    'a,b,answer',
    [[1,1,1],
     [2,2,4],
     [3,4,12],
     [5,6,30]])
def test_multiply(a,b,answer):
    assert functions._multiply(a,b)==answer
```
where now we are testing for four combinations of inputs instead of one. Parametering is very important to do if you want to write a test that covers a function that has a wide paramater space. 

### Patching and testing calls to functions

Perhaps we don't care about the output of a function but only that it is called with the correct arguments. This can be incredibly helpful when working with functions that call other functions, working with a function that is outside your codebase (like gracedb when writing a test in gwcelery for example), or the functions are mock objects and don't have an output. This can be done by using `.assert_called()` and a multitude of other functions documented [here](https://docs.python.org/3/library/unittest.mock.html) as in the following example

```
@patch('functions._add')
@patch('functions._multiply')
def test_dotproduct_calls(mock_multiply, mock_add):
    a = [1,0,1]
    b = [1,1,2]
    output = functions._dotproduct(a,b)

    # Check calls
    mock_add.assert_called()
    calls_multiply = [call(1,1),
                      call(0,1),
                      call(1,2)]
    mock_multiply.assert_has_calls(calls_multiply)
```
Here we 'patch' in mock functions in order to track how they are called before the defining the test function. We simply check that `mock_add` has been called at least once after calling `functions._dotproduct` (again recall that `_add` is used within this function.) We then check each call for `_multiply` specifically by writing out the full list of calls and verifying and putting this list in `assert_has_calls`, which will only pass if this list is exactly correct. (**Note** this effect can be achieved in a simpler manner if the function was called once with `.assert_called_once_with()` or
if we only want check one call with `.assert_called_with()`.)

### Using fixtures

We are now going to do a more complicated example by mocking up a fake version of GraceDB. This is something often done with codes in the low-latency infrastructure (see an example in [gwcelery](https://git.ligo.org/emfollow/gwcelery/blob/master/gwcelery/tests/test_tasks_superevents.py#L121-154) and in [ligo-raven](https://git.ligo.org/lscsoft/raven/blob/master/ligo/tests/test_raven_search.py#L53-131)) and necessary since we don't want to write tests that depend on what entries currently exist in GraceDB or needs an internet connection to the GraceDB server.

Let us write our function that will use GraceDB, which will grab the superevent dictionary of the corresponding GraceDB ID, in a file called `gwbroccoli.py` with the contents:

```
from ligo.gracedb.rest import GraceDb

gracedb = GraceDb('https://gracedb.ligo.org/api/')

def get_superevent(graceid, gracedb=gracedb):
    return gracedb.superevent(graceid).json()
```
Here we are using the production GraceDB server but supply alternate URLs if you wish to use a different server (e.g. the playground server at `'https://gracedb-playground.ligo.org/api/'`). We can verify this function is working by using an ipython terminal (terminal command: `ipython`) and importing it within the same directory, which should look like the following:
```
In [1]: import gwbroccoli                                                                                                                                                                                                                                                                                                                                                   

In [2]: gwbroccoli.get_superevent('S190426c')                                                                                                                                                                                                                                                                                                                               
Out[2]: 
{'superevent_id': 'S190426c',
 'gw_id': None,
 'category': 'Production',
 'created': '2019-04-26 15:22:15 UTC',
 'submitter': 'emfollow',
 'preferred_event': 'G330687',
 't_start': 1240327332.331668,
 't_0': 1240327333.348145,
 't_end': 1240327334.353516,
 'gw_events': ['G330695',
  'G330694',
  'G330693',
  'G330692',
  'G330691',
  'G330690',
  'G330689',
  'G330688',
  'G330687',
  'G330686',
  'G330685',
  'G330684',
  'G330683'],
 'em_events': [],
 'far': 1.94694181763355e-08,
 'labels': ['PE_READY',
  'ADVOK',
  'SKYMAP_READY',
  'EMBRIGHT_READY',
  'PASTRO_READY',
  'DQOK',
  'GCN_PRELIM_SENT'],
 'links': {'files': 'https://gracedb.ligo.org/api/superevents/S190426c/files/',
  'logs': 'https://gracedb.ligo.org/api/superevents/S190426c/logs/',
  'self': 'https://gracedb.ligo.org/api/superevents/S190426c/',
  'labels': 'https://gracedb.ligo.org/api/superevents/S190426c/labels/',
  'voevents': 'https://gracedb.ligo.org/api/superevents/S190426c/voevents/',
  'emobservations': 'https://gracedb.ligo.org/api/superevents/S190426c/emobservations/',
  'events': 'https://gracedb.ligo.org/api/superevents/S190426c/events/'}}
```
Since our function seems to be behaving correctly, let us build a unittest for it. We can build a mock GraceDB by creating a class with the same properties as it, in this case eventually returning a superevent dictionary when `gracedb.superevent` is called. Since we use `.json()` at the end of our function call in `gwbroccoli.py`, we can define another class that has the property `.json()` that returns the actual dictionary. The following code (which we will call `test_gwbroccoli.py`) is an example of this:
```
import gwbroccoli


class MockGracedb(object):
    def superevent(self, graceid):
        return SResponse(graceid)

class SResponse(object):
    def __init__(self, graceid):
        self.graceid = graceid
    def json(self):
        return {"superevent_id": self.graceid}


def test_get_superevent():
    superevent = gwbroccoli.get_superevent('S1', gracedb=MockGracedb())
    assert superevent['superevent_id'] == 'S1'
```
In this test we point to the `MockGracedb` class we created above when calling `gwbroccoli.get_superevent` and check that we get back a dictionary with the correct `superevent_id`.

### Bonus: Checking approximate outputs

If we wanted to verify the output of a function that only approximates an answer (perhaps we are doing some numerical work) we can use `math.isclose()` as in the following example:

```
def test_dotproduct():
    a = [1,1.01,1]
    b = [1,0.99,1]
    output = functions._dotproduct(a,b)
    assert isclose(output, 3., abs_tol=.001)
```
The dot product gives `2.9999` which is very close to `3`, which we check by asserting that `isclose()` returns `True`, which occurs if `|output - 3.| < abs_tol`. **Note** you can also use `rel_tol` if you wanted to use a relative tolerance instead.

### Bonus: Run tests automatically in GitLab

Now let's suppose we want to really take advantage of the automatic tests by having them run whenver making changes to the code in GitLab. In the main directory we need to create a file called `.gitlab-ci.yml` (already been done in our case) with at least the following:
```
image: python:3.6-stretch

test:
stage: test
before_script:
- pip install --upgrade pip
- pip install .
script:
- python setup.py test
```
This tells GitLab that there needs to be a stage called 'test', which will start by installing all the necessary packages and then execture the unittests using the same command as we've been using, `python setup.py test`. The image points to a python image so that you can use pip and python here. **Note:** you will need to make sure all the packages you need to get installed in this first step actually do get installed, which means you may need to modify `requirements.txt` and `setup.py` to require these packages upon installing.

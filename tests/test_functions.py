import functions
import pytest
from unittest.mock import call, patch
from math import isclose


def test_add():
    assert functions._add(2,2)==4


@pytest.mark.parametrize(
    'a,b,answer',
    [[1,1,1],
     [2,2,4],
     [3,4,12],
     [5,6,30]])
def test_multiply(a,b,answer):
    assert functions._multiply(a,b)==answer


@patch('functions._add')
@patch('functions._multiply')
def test_dotproduct_calls(mock_multiply, mock_add):
    a = [1,0,1]
    b = [1,1,2]
    output = functions._dotproduct(a,b)

    # Check calls
    mock_add.assert_called()
    calls_multiply = [call(1,1),
                      call(0,1),
                      call(1,2)]
    mock_multiply.assert_has_calls(calls_multiply)


def test_dotproduct():
    a = [1,1.01,1]
    b = [1,0.99,1]
    output = functions._dotproduct(a,b)
    assert isclose(output, 3., abs_tol=.001)

from ligo.gracedb.rest import GraceDb

gracedb = GraceDb('https://gracedb.ligo.org/api/')

def get_superevent(graceid, gracedb=gracedb):
    return gracedb.superevent(graceid).json()

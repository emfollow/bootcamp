import gwbroccoli


class MockGracedb(object):
    def superevent(self, graceid):
        return SResponse(graceid)

class SResponse(object):
    def __init__(self, graceid):
        self.graceid = graceid
    def json(self):
        return {"superevent_id": self.graceid}


def test_get_superevent():
    superevent = gwbroccoli.get_superevent('S1', gracedb=MockGracedb())
    assert superevent['superevent_id'] == 'S1'

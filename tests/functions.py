def _add(a,b):
    return a + b
    
def _multiply(a,b):
    return a * b
    
def _dotproduct(a,b):
    if len(a)==len(b):
        sum = 0
        for i in range(len(a)):
            sum = _add(sum, _multiply(a[i], b[i]))
        return sum
    else:
        raise AssertionError('Arrays must be equal length')

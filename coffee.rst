Coffee break
============


.. figure:: https://media.giphy.com/media/l2SpXensc5RhNzfK8/giphy.gif
   :width: 600

   Take a break and have some coffee.

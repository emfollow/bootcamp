# Python ecosystem (09:15-10:00, Chatterjee)
Partly homework to get a working Python 3.6 dev environment set up on your
machine.

## Virtual environments
Virtual environment is, as the name suggests, a local collection of software and
dependencies which can be run irrespective of the software you have in you system globally.
As an example consider the following cases

- You have Python 2.7 installed on your laptop but want to develop code in Python 3
(the two versions have significant differences and code written for one will not run on another).
You may not want to upgrade to Python 3 altogether since you still want your old codes to run,
but want to **switch** to Python 3 whenever necessary.

- Another one: Maybe you work on a machine where you do not have root access, the
typical case of working and submitting jobs on a cluster. You might want Python 3
but the administrator may not be convinced.

In either case, it is helpful to have an **environment** where you have Python 3 running
locally, not interacting with the rest of the software in your system. This is accomplished
using a __virtual environment__. When you activate this environment, the system looks for
commands within this environment. So while you have Python 2.x installed on your laptop, you
could create a virtual environment for Python 3 and develop your codes!

### Creating a virtual environment using `virtualenv`

**Note:** Since python 3.3, the `virtualenv` tool has been moved inside the `venv` module in python.
Although not useful for `python > 3.5`, it is still mentioned for completeness.

The first useful step is to create a directory where you would store your virtual environments.
You could do this while on your home directory:

```shell
deep@debian:~$ mkdir virtualenvs
```
and create all your virtual environments here as a good practice. You need `virtualenv` to create
virtual environments. If you do not have it installed, you would need to install it. It is usually
installed in clusters. You could check if it is installed using
```shell
deep@debian:~$ which virtualenv
/usr/local/bin/virtualenv
```
this should ouput the path where the command is present as shown above.
To install it, for example in Ubuntu or Debian, you could run

```shell
deep@debian:~$ sudo apt-get install virtualenv
```
or, if you have `aptitude` installed,
```shell
deep@debian:~$ aptitude install virtualenv
```
Now change to the directory you just created to store your virtual environments and create a
Python 3 environment as follows:
```shell
deep@debian:~/virtualenvs$ virtualenv -p python3 python3_env
Running virtualenv with interpreter /usr/bin/python3
Using base prefix '/usr'
New python executable in /home/deep/virtualenvs/python3_env/bin/python3
Also creating executable in /home/deep/virtualenvs/python3_env/bin/python
Installing setuptools, pip, wheel...done.
```
As you can see here using the `-p` flag, we specify that we would want to create a Python 3
environment. You could change the number 3 to something that you wanted to install. Not supplying
a `-p` flag creates a Python 2.7 environment by default. From the output, we see that some basic
tools are installed while creating the virtual environment, one of them being `pip`, the standard
python package installer. This will help us in installing other python libraries inside the virtual
environment with ease.

#### Activating the virtual environment
In order to start the virtual environment, you execute the following command:
```shell
deep@debian:~/virtualenvs/python3_env$ source bin/activate
(python3_env) deep@debian:~/virtualenvs/python3_env$
```
This changes the bash prompt as shown above. When you look for python now, it should point to the
executable in the virtual environment.
```shell
(python3_env) deep@debian:~/virtualenvs/python3_env$ which python
/home/deep/virtualenvs/python3_env/bin/python
```
Running `python` on the terminal now will start the python 3 interpreter. Any code that you develop
can be run now after activation of this virtual environment.

To deactivate the environment and return to your original shell prompt, just type `deactivate`.

### Creating a virtual environment using `venv`
In case you have `python3.6` (or greater, I have `python3.7`) installed in your system, you could also
use the `venv` module to create virtual environments. In fact, this is the recommended way going forward.
```shell
deep@debian:~/virtualenvs$ python3.7 -m venv test-3.7-env
deep@debian:~/virtualenvs$ source test-3.7-env/bin/activate
(test-3.7-env) deep@debian:~/virtualenvs$ python --version
Python 3.7.3
(test-3.7-env) deep@debian:~/virtualenvs$ deactivate 
deep@debian:~/virtualenvs$ 
```
### In case you do not have a system installation of `python >= 3.6`
Standard OS installation might not have the more modern versions of python installed by default. A
simple way to check this is to simply trying to auto-complete (hit double `Tab` after typing `python`
in ther terminal). In my case, I have the following,
```shell
deep@debian:~/virtualenvs$ python
python            python2.7         python2-config    python3.5         python3m          pythontex         
python2           python2.7-config  python3           python3.5m        python-config     pythontex3  
```
You could, alternatively, look inside your system `bin`
```shell
deep@debian:~/virtualenvs$ ls /usr/bin/python*
/usr/bin/python   /usr/bin/python2.7         /usr/bin/python2-config  /usr/bin/python3.5   /usr/bin/python3m       /usr/bin/pythontex
/usr/bin/python2  /usr/bin/python2.7-config  /usr/bin/python3         /usr/bin/python3.5m  /usr/bin/python-config  /usr/bin/pythontex3
```
As you can see, I don't have `python3.6` in my system. Thus, I need to install it. A non-invasive,
self-contained way to do this is through [Miniconda](https://docs.conda.io/en/latest/miniconda.html).
This has installation instructions for Linux, Windows and MacOSX. Also, the content of this page keeps
changing as new python versions come out. At the time of writing, it is `python3.7`. While the
installation instructions are different for different platform, all of them have an executable that
you have to download and run. For example in my case, I used the 64-bit Linux installer:
```shell
deep@debian:~/virtualenvs$ bash /home/deep/Downloads/Miniconda3-latest-Linux-x86_64.sh

Welcome to Miniconda3 4.7.10

In order to continue the installation process, please review the license
agreement.
Please, press ENTER to continue
>>> 
```
The installer will ask for a directory to where `miniconda` will be installed. The default option of
creating a directory in the `${HOME}` is fine in my opinion. However, after the installation of the
required files, it asks where you would like to modify your `.bashrc` to prepend the miniconda `bin`
directory to your `${PATH}`. I prefer **not** doing this since this overrides the system python when
you open a terminal. Feel free to do it in case you prefer. Instead I prepend this path manually,
```shell
deep@debian:~/virtualenvs$ export PATH=/home/deep/miniconda3/bin:$PATH
```
Now if you do the same exercise as above for searching for which `python` you have, you should see
something like the following,
```shell
deep@debian:~/virtualenvs$ python
python             python2.7          python2-config     python3.5          python3.7          python3.7m         python3-config     python-config      pythontex3         
python2            python2.7-config   python3            python3.5m         python3.7-config   python3.7m-config  python3m           pythontex          
```
### Virtual environment using `conda`
One could also use `conda`  directly to create virtual environments. In order to create a virtual environment
with `python3.6`, use the following,
```shell
deep@debian:~/miniconda3/envs$ conda create -n bootcamp-py36 python=3.6
```
This will create a directory called `bootcamp-py36` inside the `miniconda3/envs` directory.
Also, the way to _source_ miniconda virtual environments is slightly different.
```shell
deep@debian:~/miniconda3/envs$ source activate bootcamp-py36
```
In order to deactivate, use
```shell
(bootcamp-py36) deep@debian:~/miniconda3/envs$ source deactivate
```

## The standard library and package installation using `pip`

This sections assumes you have a working `python >= 3.6` virtual environment (see
[above section](#virtual-environments)). The python standard library contains the basic
functionality provided by python listed [here](https://docs.python.org/3/library/index.html).
The python packaging index, [PyPI](https://pypi.org/), is the repository of additional third-party
packages which are often used in the scientific community.
Some of the relevant packages include `numpy`, `scipy`, `astropy` etc. These can be installed using
the package name so long as it exists in PyPI. Ensure that you are in the desired virtual environment
and that you are using the `pip` present in the virtual environment.
```shell
(test-3.7-env) deep@debian:~/virtualenvs$ which pip
/home/deep/virtualenvs/test-3.7-env/bin/pip
```
In its simplest usage, installation using `pip` could be done simply using the package name,
```shell
(test-3.7-env) deep@debian:~/virtualenvs$ pip install numpy
Looking in indexes: https://pypi.org/simple, https://packagecloud.io/github/git-lfs/pypi/simple
Collecting numpy
  Downloading https://files.pythonhosted.org/packages/ba/e0/46e2f0540370f2661b044647fa447fef2ecbcc8f7cdb4329ca2feb03fb23/numpy-1.17.2-cp37-cp37m-manylinux1_x86_64.whl (20.3MB)
    100% |████████████████████████████████| 20.3MB 3.8MB/s 
Installing collected packages: numpy
Successfully installed numpy-1.17.2
```
Use the `freeze` subcommand to list the installed packages.
```shell
(test-3.7-env) deep@debian:~/virtualenvs$ pip freeze
numpy==1.17.2
```
Usually, any third-party package could have many dependencies which could be put into a single file, typically called `requirements.txt`. An example content could be
```
numpy == 1.15
scipy < 1.2.0
astropy
```
Note the version specifiers one could use. To install all the requirements,
```shell
(test-3.7-env) deep@debian:~/virtualenvs$ pip install -r requirements.txt 
```
### Installing from a git SHA
An alernate usage of `pip` could also be used to install python packages directly from version control,
like `git`. For example, if you want a development version of `scipy` that is not released yet.
This could be the case when, say, you want to use a new feature of `scipy` which is present in the
[scipy master](https://github.com/scipy/scipy/commits/master) but the scipy team will not make
a stable release until they have tested out their software end to end. In that case, you can install
directly from __a commit__ instead of __a version__.
```shell
(test-3.7-env) deep@debian:~/virtualenvs$ pip install git+https://github.com/scipy/scipy.git@35a284e59e57a440b155fe31fc35589434d688d0
(test-3.7-env) deep@debian:~/virtualenvs$ pip freeze
astropy==3.2.1
numpy==1.15.0
scipy==1.4.0.dev0+35a284e
```
Note the output of `pip freeze` this time.

### The `-e` argument for `pip`
In case you would like to develop a python package, you would want to __install__ and __run__ your
code every time you make a change. Python will discover the packages which are present in the `lib`
directory of your virtual environments. For example, if you installed the packages from the
`requirements.txt` from above, you should see something like, 
```shell
(test-3.7-env) deep@debian:~/virtualenvs$ ls test-3.7-env/lib/python3.7/site-packages/
astropy                  easy_install.py  numpy-1.15.0.dist-info  pip-19.2.3.dist-info  __pycache__  scipy-1.4.0.dev0+35a284e.dist-info  setuptools-40.8.0.dist-info
astropy-3.2.1.dist-info  numpy            pip                     pkg_resources         scipy        setuptools
```
It would be desirable to have the package you wish to develop to be soft-linked here in order to
reflect the changes. This is achieved using the  the `-e, --editable` optional argument. Suppose
you wish to develop [gwcelery](https://git.ligo.org/emfollow/gwcelery) and you have cloned it into
the directory `~/ligogit/forks/gwcelery`. You should install the package as follows,
```shell
(test-3.7-env) deep@debian:~/ligogit/forks/gwcelery$ pip install -e .
```
This should create soft links to the source files present, so that any development you do is
immediately refected.

## The `pipenv` tool and `Pipfile.lock`
### Deterministic builds with an example
Imagine you are developing a package that needs the `scikit-learn` library as a dependency. Since new versions are
released pretty frequently, one can think of a situation where the release of a new version results in causes a regression
in your code. One good practice is to pin the verion, say, `scikit-learn == 0.19.2` in your `requirements.txt` file.
But `scikit-learn` might have its own dependencies which might change. To make your build deterministic, it is
important to __lock__ the complete dependency tree, only updating it carefully when new versions are available.
This is achieved by the `pipenv` tool which creates two files
- `Pipfile`
- `Pipfile.lock`

The `Pipfile` is analogous to the `requirements.txt`. The `Pipfile.lock` contains a detailed dependency tree for
the entire project.

Consider that you have completed the development of a particular version of a project and are ready to make a
release. Let us consider a project with the following content under `requirements.txt`:
```shell
astropy
lalsuite
numpy
pandas
python-ligo-lw
scikit-learn==0.19.2
scipy
h5py
```
Install `pipenv` inside of your virtual environment  (`pip install pipenv`) and then install using `pipenv`
as follows:
```shell
(test-3.7-env) deep@debian:~/ligogit/forks/p-astro$ pipenv install -r requirements.txt 
Courtesy Notice: Pipenv found itself running within a virtual environment, so it will automatically use that environment, instead of creating its own for any project. You can set PIPENV_IGNORE_VIRTUALENVS=1 to force pipenv to ignore that environment and create its own instead. You can set PIPENV_VERBOSITY=-1 to suppress this warning.
Creating a Pipfile for this project…
Requirements file provided! Importing into Pipfile…
Pipfile.lock not found, creating…
Locking [dev-packages] dependencies…
Locking [packages] dependencies…
✔ Success! 
Updated Pipfile.lock (6dd43f)!
Installing dependencies from Pipfile.lock (6dd43f)…
  🐍   ▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉ 23/23 — 00:00:02
```
This will create the `Pipfile` and `Pipfile.lock`. You will see that the `Pipfile` is simply a reflection of your `requirements.txt`.
However, the `Pipfile.lock` has snippets like the following:
```console
    "default": {
        "asn1crypto": {
            "hashes": [
                "sha256:2f1adbb7546ed199e3c90ef23ec95c5cf3585bac7d11fb7eb562a3fe89c64e87",
                "sha256:9d5c20441baf0cb60a4ac34cc447c6c189024b6b4c6cd7877034f4965c464e49"
            ],
            "version": "==0.24.0"
        },
        "astropy": {
            "hashes": [
                "sha256:02bdd19313110601d29d65f1a5cbcd735050f491bdeed516c062209302dcc9c3",
                "sha256:069526e684f8297e97befdd352a51d09ca1274880fb39495c2afb52bb51aac45",
                "sha256:11e169b4a4077e77821595d16927df0fa47b7c5b9ed9065e394374962b4bc58a",
                "sha256:1fbb54f45c1ef44fc632e76b299af0f2932c87fd501b7161d684311892bddd14",
                "sha256:25c001f8f5047d820be2cc4aa86750c6010a0f67871680d35d838ecde3e85288",
                "sha256:6f1efd2e9ba505d9faa05becddef410d9551a50af09f6fed05dababa28ab2ad5",
                "sha256:706c0457789c78285e5464a5a336f5f0b058d646d60f4e5f5ba1f7d5bf424b28",
                "sha256:724bbacce4b72d4a4158d24ad3cafc7ac15f844a65128d03f674f93e36adf47c",
                "sha256:7308e28c7c1f5e60d564bc6edeee56a8070fef0736dc5419e0fe17261af80e49",
                "sha256:839bef4f44541c469f70206b7e089fb93247d799d20f0ba0868d62375011d692",
                "sha256:95788303b6ba5403a0de9f4eef713a75d62d21a9831e1bb939f912f11b7d3604",
                "sha256:a77771092664bdea0755af9fb77cac7523b01138983751249cc7673489822d25",
                "sha256:a9e8073857895fba94590e763d5c9b4e7e19a24dec8c11708c5a6986a87d0d1a",
                "sha256:b5ca066e2831cdf376dc93d56be57f915ee2b290e9f66254d89b990ddc75f604",
                "sha256:e642f0957ea0d48d7da7c7d0ff38d271eb71edfe5238033a5e5f16fafe06a5e2",
                "sha256:ede59589859b5926d405a5e71c5454388f7b071ac6e9a1abad252e47c29ea1cf"
            ],
            "index": "pypi",
            "version": "==3.2.1"
```
Note how it has frozen the versions for each dependency and their sub dependency, thus, having
a **deterministic build**. As a developer, you will supply the `Pipfile(.lock)` in version
control, so that the complete developer team can install the same build for development.

### Installing from the Pipfile
If a project supplies a `Pipfile.lock`, you can install the dependencies using
```shell
(test-3.7-env) deep@debian:~/ligogit/forks/p-astro$ pipenv install
Courtesy Notice: Pipenv found itself running within a virtual environment, so it will automatically use that environment, instead of creating its own for any project. You can set PIPENV_IGNORE_VIRTUALENVS=1 to force pipenv to ignore that environment and create its own instead. You can set PIPENV_VERBOSITY=-1 to suppress this warning.
Installing dependencies from Pipfile.lock (6dd43f)…
  🐍   ▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉ 23/23 — 00:00:02
```
which installs all dependencies from `Pipfile.lock` and gives you a deterministic build to develop
each time.

### Updating the Pipfile
It is natural that new versions of (sub)dependencies will be released frequently. Therefore, once
in a while the `Pipfile(.lock)` needs to be updated. It might also be that you updated, say, a new
version of `astropy` but find (later) that it results in a bug, and, as a result have to downgrade
it. Such cases would involve updating the entire dependency tree. This is achieved by the `update`
sub command.

For it to make sense, let us change the version of `astropy` in `requirements.txt` file we were
using in the [previous section](#deterministic-builds-with-an-example)
```console
astropy==3.1.2
lalsuite
numpy
pandas
python-ligo-lw
scikit-learn==0.19.2
scipy
h5py
```
where we have pinned the `astropy==3.1.2` version to an earlier version. You also make a change in the
`Pipfile`. Change its contents as follows:
```console
[[source]]
name = "pypi"
url = "https://pypi.org/simple"
verify_ssl = true

[dev-packages]

[packages]
astropy = "==3.1.2"
lalsuite = "*"
numpy = "*"
pandas = "*"
python-ligo-lw = "*"
scikit-learn = "==0.19.2"
scipy = "*"
h5py = "*"

[requires]
python_version = "3.7"
```
Now run `pipenv update`
```shell
(test-3.7-env) deep@debian:~/ligogit/forks/p-astro$ pipenv update --clear
Courtesy Notice: Pipenv found itself running within a virtual environment, so it will automatically use that environment, instead of creating its own for any project. You can set PIPENV_IGNORE_VIRTUALENVS=1 to force pipenv to ignore that environment and create its own instead. You can set PIPENV_VERBOSITY=-1 to suppress this warning.
Running $ pipenv lock then $ pipenv sync.
Locking [dev-packages] dependencies…
Locking [packages] dependencies…
✔ Success! 
Updated Pipfile.lock (5975ca)!
Installing dependencies from Pipfile.lock (5975ca)…
  🐍   ▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉ 23/23 — 00:00:03
To activate this project's virtualenv, run pipenv shell.
Alternatively, run a command inside the virtualenv with pipenv run.
All dependencies are now up-to-date!
```
now the `Pipfile.lock` should reflect the change.
```console
        "astropy": {
            "hashes": [
                "sha256:0572baf6ff436daceb0a9b796a8f83904afc8acd0bb5490e67cde059d440e432",
                "sha256:0c77f59f578794c9984875e2a4413ca771432dc1e64d7c4ec0a49ded33f68a60",
                "sha256:10eac60bea96f5fe7bfa785ef7fcbbedc040d9304242a04f4884218f5dfb0293",
                "sha256:2204726f369d51834ab46cdb016b2a1f9bd12e115e6bf21333120a3b0fab8d25",
                "sha256:4a78a8ec9666d0a51a37f03494aaa5012e241ba37053e6c913c039cddee89ede",
                "sha256:50e27c37a1168fd4b729607ecdd9a29ba8d89f901d4b3c53a63fa88ab9818cbd",
                "sha256:5a1bf7f52c4c6ee72190bfb1f205c993a4bbcbcae86c79e618b1a08de356d395",
                "sha256:79f5456d28f9c10893840bf9b8018c96f46a8c165416269e3a80242b82ecc8ab",
                "sha256:814495f20e1320955a27564acfb64ba17f9a10f3578365c3be4b9bf8c3c49934",
                "sha256:a803f4b5a8193d51b7e0109019576a0c1f15b79d6b54a9d784664c2b4c97fb0d",
                "sha256:b27d55543edb2032fddc3f77fef86d202ca6bd2134413746d2c0c29d359bf89b",
                "sha256:b5d84d2644399c2342c941fc4f188bdb4ab2070a84edf59d54a266386458c74b",
                "sha256:bfd143d680324c63c12a687a3defba736fd80ea9cb643729ff751e62c3a6246d",
                "sha256:c814efb88181c707b836e559c931ce800d176d8f66b16271dbf959cabff9cc79",
                "sha256:cc776d57c8a64e7c2eae99e6a6cc20990e1791dbb2aa6a4bde665e9a6eba3963",
                "sha256:efa11e5852c2f0f6e15efbf53151a141e4417b956ff94bb0f1db708acb5f085c"
            ],
            "index": "pypi",
            "version": "==3.1.2"
        },
```
More importantly, note that the above change might not be the only change, as it might
affect the whole dependency tree. A full `diff` between the two versions of the
`Pipfile.lock`, in this case, due to this change is,
```console
@@ -1,7 +1,7 @@
 {
     "_meta": {
         "hash": {
-            "sha256": "af9ba9b57feaf0b0f971218bbb0afe47849f21c25d20e6cc4b758707ee6dd43f"
+            "sha256": "23341677c954061b4a2fa39855c923c13b3d37f962c679a4e3ff5f5b875975ca"
         },
         "pipfile-spec": 6,
         "requires": {
@@ -25,25 +25,25 @@
         },
         "astropy": {
             "hashes": [
-                "sha256:02bdd19313110601d29d65f1a5cbcd735050f491bdeed516c062209302dcc9c3",
-                "sha256:069526e684f8297e97befdd352a51d09ca1274880fb39495c2afb52bb51aac45",
-                "sha256:11e169b4a4077e77821595d16927df0fa47b7c5b9ed9065e394374962b4bc58a",
-                "sha256:1fbb54f45c1ef44fc632e76b299af0f2932c87fd501b7161d684311892bddd14",
-                "sha256:25c001f8f5047d820be2cc4aa86750c6010a0f67871680d35d838ecde3e85288",
-                "sha256:6f1efd2e9ba505d9faa05becddef410d9551a50af09f6fed05dababa28ab2ad5",
-                "sha256:706c0457789c78285e5464a5a336f5f0b058d646d60f4e5f5ba1f7d5bf424b28",
-                "sha256:724bbacce4b72d4a4158d24ad3cafc7ac15f844a65128d03f674f93e36adf47c",
-                "sha256:7308e28c7c1f5e60d564bc6edeee56a8070fef0736dc5419e0fe17261af80e49",
-                "sha256:839bef4f44541c469f70206b7e089fb93247d799d20f0ba0868d62375011d692",
-                "sha256:95788303b6ba5403a0de9f4eef713a75d62d21a9831e1bb939f912f11b7d3604",
-                "sha256:a77771092664bdea0755af9fb77cac7523b01138983751249cc7673489822d25",
-                "sha256:a9e8073857895fba94590e763d5c9b4e7e19a24dec8c11708c5a6986a87d0d1a",
-                "sha256:b5ca066e2831cdf376dc93d56be57f915ee2b290e9f66254d89b990ddc75f604",
-                "sha256:e642f0957ea0d48d7da7c7d0ff38d271eb71edfe5238033a5e5f16fafe06a5e2",
-                "sha256:ede59589859b5926d405a5e71c5454388f7b071ac6e9a1abad252e47c29ea1cf"
+                "sha256:0572baf6ff436daceb0a9b796a8f83904afc8acd0bb5490e67cde059d440e432",
+                "sha256:0c77f59f578794c9984875e2a4413ca771432dc1e64d7c4ec0a49ded33f68a60",
+                "sha256:10eac60bea96f5fe7bfa785ef7fcbbedc040d9304242a04f4884218f5dfb0293",
+                "sha256:2204726f369d51834ab46cdb016b2a1f9bd12e115e6bf21333120a3b0fab8d25",
+                "sha256:4a78a8ec9666d0a51a37f03494aaa5012e241ba37053e6c913c039cddee89ede",
+                "sha256:50e27c37a1168fd4b729607ecdd9a29ba8d89f901d4b3c53a63fa88ab9818cbd",
+                "sha256:5a1bf7f52c4c6ee72190bfb1f205c993a4bbcbcae86c79e618b1a08de356d395",
+                "sha256:79f5456d28f9c10893840bf9b8018c96f46a8c165416269e3a80242b82ecc8ab",
+                "sha256:814495f20e1320955a27564acfb64ba17f9a10f3578365c3be4b9bf8c3c49934",
+                "sha256:a803f4b5a8193d51b7e0109019576a0c1f15b79d6b54a9d784664c2b4c97fb0d",
+                "sha256:b27d55543edb2032fddc3f77fef86d202ca6bd2134413746d2c0c29d359bf89b",
+                "sha256:b5d84d2644399c2342c941fc4f188bdb4ab2070a84edf59d54a266386458c74b",
+                "sha256:bfd143d680324c63c12a687a3defba736fd80ea9cb643729ff751e62c3a6246d",
+                "sha256:c814efb88181c707b836e559c931ce800d176d8f66b16271dbf959cabff9cc79",
+                "sha256:cc776d57c8a64e7c2eae99e6a6cc20990e1791dbb2aa6a4bde665e9a6eba3963",
+                "sha256:efa11e5852c2f0f6e15efbf53151a141e4417b956ff94bb0f1db708acb5f085c"
             ],
             "index": "pypi",
-            "version": "==3.2.1"
+            "version": "==3.1.2"
         },
         "cffi": {
             "hashes": [
```
Note that apart from the checksums of `astropy` being updated, there are also changes
to the checksums of the sub-dependency `_meta`.

### See the dependency tree
Use the `graph` subcommand to see a nice visual representation. A few lines of output are
shown from our [example above](#deterministic-builds-with-an-example)
```shell
(test-3.7-env) deep@debian:~/ligogit/forks/p-astro$ pipenv graph
astropy==3.1.2
  - numpy [required: >=1.13.0, installed: 1.17.2]
h5py==2.10.0
  - numpy [required: >=1.7, installed: 1.17.2]
  - six [required: Any, installed: 1.12.0]
```

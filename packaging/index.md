# Releasing a Package (16:00-16:30, Piotrzkowski)
Homework: check out some related tutorials :
- How to upload your package to PyPi: [https://medium.com/@joel.barmettler/how-to-upload-your-python-package-to-pypi-65edc5fe9c56](https://medium.com/@joel.barmettler/how-to-upload-your-python-package-to-pypi-65edc5fe9c56)
- How to release a new version of gwcelery: [https://gwcelery.readthedocs.io/en/latest/deployment.html](https://gwcelery.readthedocs.io/en/latest/deployment.html)

You may need to install the following packages:
- wheel
- twine

If you haven't already, clone the bootcamp git repository using:
```
https://git.ligo.org/brandon.piotrzkowski/gwbroccoli
```

## Before making a release

This section is based heavily on [this linked tutorial](https://medium.com/@joel.barmettler/how-to-upload-your-python-package-to-pypi-65edc5fe9c56).

### Setting up your respository

I have set up a repository we can do this release with at [https://git.ligo.org/brandon.piotrzkowski/gwbroccoli](https://git.ligo.org/brandon.piotrzkowski/gwbroccoli).

Note the presence of a few files: `setup.py`, `setup.cfg`, `LICENSE.txt`, and `CHANGES.md`, which are in the main directory, while all of the code we want to release is in it's own folder `gwbroccoli` (there is much more to this but we won't go into this here; see the tutorial linked above). While there are more files that can be included here (and are in this repository), these are the minimum we will consider required:

**``setup.py``**:
```
from setuptools import setup

setup(
    name='gwbroccoli',
    version='0.9.0',
    author='Brandon Piotrzkowski',
    author_email='brandon.piotrzkowski@ligo.org'
    description='An example repository to demonstrate how to release a python package',
    license='MIT',
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    install_requires=['ligo-gracedb>=2.2.0'],
    python_requires='>=3.6'
)

```
Here we define the metadate of our python package, from version numbers and authors to python and package requirements.

**``setup.cfg``**:
```
[aliases]
test=pytest
```
Here we set up the configuration, pointing towards our tests using pytest.

**``LICENSE.txt``**:
```
MIT License

Copyright (c) 2019 BRANDON PIOTRZKOWSKI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
Here we define the license for our package, for what the allowed use, modification, and distribution should be.

**``CHANGES.md``**:
```
# Changelog

## 0.9.0 (unreleased)

-   Add tests for every function.

-   Add add, multiply, dot product, and get superevent functions.

-   Initialize repository.
```
Here we catalogue the changes done to our code, catagorized into every release. We will be modifying this file in just a bit.

### Get your code reviewed before making a release

**See subject**

But really. This includes having peers take a look at your code **before** it merged to the master branch, as well as having a review committee check and sign off on your package (if you are unsure of who should do this please consult the emfollow chairs.)

## Preparing a Release

This section is based heavily on [the gwcelery tutorial](https://gwcelery.readthedocs.io/en/latest/deployment.html). This is one of the resources I used when learning this, so please check it out if you have time.

### Verify the output of your code

**Try not to release code that has bugs, especially those that could've been found with a little bit of testing.**  While bugs can seem inevitable, the vast majority can be prevented by simple tests, both on the level of the individual function and end-to-end tests involving entire pipelines.

This procedure will vary wildly between different respositories. If you have automated unittests instated, make sure they are all passing on the latest commit. If you have other manual tests, make sure they pass. For example for `ligo-raven` we always create external events near superevents in gracedb-playground and make sure the full RAVEN pipeline runs correctly (external event is found in coincidence, `EM_COINC` label is applied, coincidence FAR is calculated.)

### Clean up and update your changelog; update version number

**Make sure that the changes made to the code are reflected in your changelog. No more, no less.** 

This may mean going through the commit history and making sure the entries in the changelog reflect these commits, possibly adding entries where other developers may have forgotten to archive their changes. You must also update the date next to the version you are releasing (typically this means replacing `unreleased` with the current date). 

Also check that the version number has been updated to the new number, usually changed by modifying version field in `setupy.py` in the MAJOR.MINOR.PATCH convention. **Note:** Typically we update PATCH for small updates, MINOR for fairly minor changes, and MAJOR for large fundamental changes. Packages can also have different numbering systems, e.g. [`ligo-raven`](https://git.ligo.org/lscsoft/raven) uses a MAJOR.MINOR system. See [this tutorial](https://www.python.org/dev/peps/pep-0440/) for more information on python package versioning conventions.

When this is all done, commit the changes with the message `Update CHANGES.md`.

**Note:** packages with `versioneer.py` don't need their version number updated since this is handled when the tag is applied in the next step.

### Tag and release the code to PyPi

Now that all the files have been prepared we can apply a tag that will store this version of this code in GitLab in case you wish to make comparisons with future code or reinstate this version. This is done by the following

```console
brandonpiotrzkowski$ git tag vMAJOR.MINOR.PATCH -m "Version MAJOR.MINOR.PATCH"
```
where you place the appropriate numbers in MAJOR, MINOR, and PATCH to match what you chose above for the new version number.

You can push the changes from updating the changlog earlier and the tag we just created by
```console
brandonpiotrzkowski$ git push && git push --tags
```
where you can add 'upstream' after each push if you have set that up (where `origin` would be your fork and `upstream` is the main fork.) Next we can create a file to upload to PyPi. This can be a wheel (.whl) or a tarball (tar.gz), where we can create either using the commands in the main directory

wheel:
```console
brandonpiotrzkowski$ python setup.py bdist_wheel
```
tarball:
```console
brandonpiotrzkowski$ python setup.py sdist
```
For this tutorial we will use a tarball. We can make sure this is installable by entering the `dist`  directory and using pip

```console
brandonpiotrzkowski$ pip install tar_ball_filename.tar.gz
```
If correctly installs, then we are ready to upload this to PyPi. We can do this via twine 
```console
brandonpiotrzkowski$ twine upload tar_ball_filename.tar.gz
```
where you will need to enter your PyPi username and password. If this succeeds, check out out the corresponding PyPi page to make sure that this new version is there.

Since we are not uploading a package to the real PyPi in this case but test.PyPi, we can use the following instead
```console
brandonpiotrzkowski$ twine upload --repository-url https://test.pypi.org/legacy/tar_ball_filename.tar.gz
```

If you have uploaded your python to normal PyPi, you can install it by the usual
```
pip install gwbroccoli
```

or if you uploaded to test PyPi
```
pip install --index-url https://test.pypi.org/simple/ gwbroccoli
```


**Note:** some packages are set up to upload new versions to PyPi and even submit tickets to SCCB automatically after a new tag is applied and pushed to master (see [gwcelery](https://git.ligo.org/emfollow/gwcelery) and [ligo-followup-advocate](https://git.ligo.org/emfollow/ligo-followup-advocate) for examples). If given an already existing repository to manage, learn it's specfic procedure to release new versions. What is presented here is the generic, 'manual' procedure to get you acquainted with the steps and possibly do this for your own repository.

### Submit a ticket to SCCB

You can create a new ticket by opening a new issue in the following GitLab page: [https://git.ligo.org/sccb/requests/issues](https://git.ligo.org/sccb/requests/issues)

You will be required to provide a changelog and `diff` since the last reviewed version (if this package has been reviewed before; contact Duncan Macleod for more questions). Your review committee will also be asked to sign-off and provide a statement that the package was reviewed. The SCCB will (hopefully) sign off and your package will be authorized to be deployed.

### Prepare for developing again

You can prepare for new changes by updating the version number as mentioned above to the *next* expected version number. Then add a new entry in the changelog like the following:

```
# Changelog

## next.version.number (unreleased)

-   No changes yet


## last.version.number (XX-XX-XXXX)

-   Previous changelog entries
```

Commit the changes with the message `Back to development`. Congrats, you're all done (once the SCCB approves the package and it is deployed)!

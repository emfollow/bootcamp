from setuptools import setup

setup(
    name='emfollow-bootcamp',
    version='0.1.0',
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    install_requires=['ligo-gracedb>=2.2.0']
)

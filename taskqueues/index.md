# Task Queues (08:30-10:00, Chatterjee)
In this session we discuss the concept of _task queues_ and discuss the
implementation of task queues using `celery`.

## Introduction
Task queues are a way to distribute the workload of an application
across resources available in your system. They let you farm your _tasks_
out to specific _workers_ so that you decide which of them run
(a)synchronously and prioritize certain tasks over others. The term _task_
refers to any computation that runs as an independent process. It could be
a simple download to fetch a file, or something more complex like performing
parameter estimation for LIGO-Virgo triggers. A _queue_ is a (constantly changing)
list of tasks that are to be executed. A _worker_ is something that will execute
a task. For simplicity, we can think of a worker as a CPU thread.
We will lift this assumption [later](#concurrency).

Let's consider the simple visualization below.

![task-queue-basic|567x329,20%](queue_worker.png)

Here you are partitioning your resources into three queues - _Queue 1_,
_Queue 2_ and _Queue 3_. Depending on the task you wish to execute,
you could place it in either of the queues. As our application becomes
more complex with a large number of tasks, we can make a judicious choice.

### Embarrassingly parallel (asynchronous) tasks
Suppose you have some embarrassingly parallel that your application
needs to compute. These are tasks that can run independently of each
other. A scenario relevant in LIGO-Virgo is to generate
skymaps for a certain number number of events, maybe the ones this week.
These could all be sent to _Queue 1_ where _10 workers_ can independently
work on _10 skymaps_ in parallel. In case you supply 20 of them, the 11th
task will be picked up by a worker when it becomes available.

### Sequential (synchronous) tasks
Complimentary to parallel tasks are tasks that are evaluated serially.
Sometimes, we would like to have only once instance of a running task.
Such tasks could be delegated to _Queue 3_ which has only a single
worker.

Thus, _task queues_ provide the flexibility to delegate tasks to
system resources. Some other flexibility include:
- Prioritizing certain tasks over others.
- Controlling tasks after it is submitted to a queue. For example,
  you want the task execution to start two minutes after submission.
- Running a certain task periodically. For example, a reminder email.
- Retrying tasks in case of failure, or, do something different if the
  task fails a certain number of times.
- Revoking tasks if necessary.

### Using `celery`
[Celery](http://www.celeryproject.org/) is one of the available distributed
task queue applications. It is written in python and is suitable for
task distribution for a python application. It is available on PyPI
and can be installed with `pip`.
```console
(test-3.7-env) deep@debian:~/work$ pip install celery
```
Celery also requires a [message broker](http://docs.celeryproject.org/en/latest/getting-started/brokers/).
This is the service that delegates messages to celery workers and
preserves the state of tasks. In our case, we will use the `redis`
broker. We need to install the redis server and launch it. Open a
new terminal (no need of using a virtual environment) to download,
build and start the server
```console
deep@debian:~$ wget http://download.redis.io/redis-stable.tar.gz
deep@debian:~$ tar xvzf redis-stable.tar.gz
deep@debian:~$ cd redis-stable
deep@debian:~/redis-stable$ make
deep@debian:~/redis-stable$ ./src/redis-server 
21508:C 30 Sep 2019 10:11:51.582 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
21508:C 30 Sep 2019 10:11:51.582 # Redis version=5.0.6, bits=64, commit=00000000, modified=0, pid=21508, just started
21508:C 30 Sep 2019 10:11:51.582 # Warning: no config file specified, using the default config. In order to specify a config file use ./src/redis-server /path/to/redis.conf
21508:M 30 Sep 2019 10:11:51.585 # You requested maxclients of 10000 requiring at least 10032 max file descriptors.
21508:M 30 Sep 2019 10:11:51.585 # Server can't set maximum open files to 10032 because of OS error: Operation not permitted.
21508:M 30 Sep 2019 10:11:51.585 # Current maximum open files is 4096. maxclients has been reduced to 4064 to compensate for low ulimit. If you need higher maxclients increase 'ulimit -n'.
                _._                                                  
           _.-``__ ''-._                                             
      _.-``    `.  `_.  ''-._           Redis 5.0.6 (00000000/0) 64 bit
  .-`` .-```.  ```\/    _.,_ ''-._                                   
 (    '      ,       .-`  | `,    )     Running in standalone mode
 |`-._`-...-` __...-.``-._|'` _.-'|     Port: 6379
 |    `-._   `._    /     _.-'    |     PID: 21508
  `-._    `-._  `-./  _.-'    _.-'                                   
 |`-._`-._    `-.__.-'    _.-'_.-'|                                  
 |    `-._`-._        _.-'_.-'    |           http://redis.io        
  `-._    `-._`-.__.-'_.-'    _.-'                                   
 |`-._`-._    `-.__.-'    _.-'_.-'|                                  
 |    `-._`-._        _.-'_.-'    |                                  
  `-._    `-._`-.__.-'_.-'    _.-'                                   
      `-._    `-.__.-'    _.-'                                       
          `-._        _.-'                                           
              `-.__.-'                                               

21508:M 30 Sep 2019 10:11:51.587 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
21508:M 30 Sep 2019 10:11:51.587 # Server initialized
21508:M 30 Sep 2019 10:11:51.587 # WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
21508:M 30 Sep 2019 10:11:51.587 # WARNING you have Transparent Huge Pages (THP) support enabled in your kernel. This will create latency and memory usage issues with Redis. To fix this issue run the command 'echo never > /sys/kernel/mm/transparent_hugepage/enabled' as root, and add it to your /etc/rc.local in order to retain the setting after a reboot. Redis must be restarted after THP is disabled.
21508:M 30 Sep 2019 10:11:51.587 * Ready to accept connections
```
Now go back to you virtual environment and install the redis client
```console
(test-3.7-env) deep@debian:~/work$ pip install redis
```
The broker also preserves the state of tasks in case of an unexpected shutdown so
that running tasks can be picked up upon a restart.

## A basic `celery` application
Let's create a separate workspace for our celery application.
```console
(test-3.7-env) deep@debian:~/work$ mkdir celery_testing
(test-3.7-env) deep@debian:~/work$ cd celery_testing
(test-3.7-env) deep@debian:~/work/celery_testing$ 
```
Let's create a file that will store the _tasks_ for our simple application.
We call it `celery_app.py` with the following content:
```console
from celery import Celery

app = Celery('tasks', broker = 'redis://')
app.conf['result_backend'] = app.conf.broker_url

@app.task
def add(x, y):
    return x + y

@app.task
def prod(x, y):
    return x * y
```

So far we have two simple tasks: `add` and `prod`. Note that they are simple
python functions except they are decorated with the `@app.task` decorator.
This decorator makes our simple function a _celery task_ which can then be
put in a _celery queue_. Notice that we are using `redis` as a broker to pass
messages and also to store the results of tasks with the `result_backend`
attribute. The choice could be different, for example, you could use _redis_
as a broker and a _SQL database_ for storing results. Here is a list of
available [backends](https://docs.celeryproject.org/en/master/userguide/configuration.html#std:setting-result_backend).

At this point, it is good idea to open another terminal and source the virtual environment
before proceeding. This terminal will be used only to launch the celery application.
Launch this application using,
```console
(test-3.7-env) deep@debian:~/work/celery_testing$ celery -A celery_app worker -l info
 
 -------------- celery@debian v4.3.0 (rhubarb)
---- **** ----- 
--- * ***  * -- Linux-4.9.0-11-amd64-x86_64-with-debian-9.11 2019-09-30 09:17:26
-- * - **** --- 
- ** ---------- [config]
- ** ---------- .> app:         tasks:0x7f9024054780
- ** ---------- .> transport:   redis://localhost:6379//
- ** ---------- .> results:     redis://
- *** --- * --- .> concurrency: 8 (prefork)
-- ******* ---- .> task events: OFF (enable -E to monitor tasks in this worker)
--- ***** ----- 
 -------------- [queues]
                .> celery           exchange=celery(direct) key=celery
                

[tasks]
  . celery_app.add
  . celery_app.prod

[2019-09-30 09:17:27,073: INFO/MainProcess] Connected to redis://localhost:6379//
[2019-09-30 09:17:27,079: INFO/MainProcess] mingle: searching for neighbors
[2019-09-30 09:17:28,097: INFO/MainProcess] mingle: all alone
[2019-09-30 09:17:28,123: INFO/MainProcess] celery@debian ready.
```
Let's break down the command-line: the `-A` stands for _application_, the `worker`
is a directive to start a _worker_, the `-l` is for the verbosity of log messages
which we set to level `info`. From the corresponding output, we see the broker and
backend set to `redis`.
### Concurrency
The _concurrency_ is the number of tasks a worker can simultaneously handle which defaults
to the number of CPUs (#cores times #threads per core) in the machine. We can set
this manually using the `-c` option. Hit `Ctrl+C` to shutdown the application and
restart using the following.
```console
(test-3.7-env) deep@debian:~/work/celery_testing$ celery -A celery_app worker -l info -c 1
```
### Queues
If you look at the output above, you will notice that by default a queue is created with
the name _celery_. This can be customized, using the `-Q` option.
```console
(test-3.7-env) deep@debian:~/work/celery_testing$ celery worker -n worker_2 -A celery_app -l info -Q queue_2 -c 2
```
One can define multiple queues this way.

## Queueing tasks
With the celery application running, now we can queue tasks. Feel free to use a jupyter
notebook or an IPython session at this stage for this. Let's use the default worker/queue
settings for now, with a concurrency of 2.
```console
(test-3.7-env) deep@debian:~/work/celery_testing$ celery worker -A celery_app -l info -c 2
```

Let's import our tasks in your jupyter-notebook, IPython session or otherwise
```python
from celery_app import add, prod
add(1, 2)  # should print 3
prod(2, 3)  # should print 6
add.delay(1, 2) # should return an AsyncResult
```
At this stage, have a look at the logs being printed in the terminal where you have the
celery application running. It should have something like,
```console
[2019-09-30 12:30:39,951: INFO/MainProcess] Received task: celery_app.add[b23d9b37-c301-439d-904f-5e7793b4ca64]  
[2019-09-30 12:30:39,956: INFO/ForkPoolWorker-1] Task celery_app.add[b23d9b37-c301-439d-904f-5e7793b4ca64] succeeded in 0.0035571619991969783s: 3
```
which implies that this was received and processed by the celery worker and finished in 0.003s.
Also note that the `.delay` is something that is a result of the function being decorated as a
_celery task_.

## Chain, Group & Canvas
With `celery` we could execute tasks in series, passing arguments from one task execution to
the next. Consider the following,
```python
chain_add = ( add.s(1, 2) | add.s(1) | add.s(2) )
chain_add.delay()
```
This an example of celery syntactic sugar. The `.s` attribute is a _signature_ which mean you
initialize the task with arguments but don't execute it. Further, note that we supplied only
**one** argument to `add` when it had **two** arguments in its function definition. With the
`.s` attribute, the second `add` takes the result of the `add(1, 2)` as its _first argument_.
Same holds for the final `add`. So the expected result is: `{(1+2) + 1} + 2 = 6`, which is
reflected in the logs.
```console
[2019-09-30 12:37:32,862: INFO/MainProcess] Received task: celery_app.add[8cbb9c63-3706-4cf3-be06-ec1edbbde68d]  
[2019-09-30 12:37:32,936: INFO/ForkPoolWorker-1] Task celery_app.add[8cbb9c63-3706-4cf3-be06-ec1edbbde68d] succeeded in 0.07239835200016387s: 3
[2019-09-30 12:37:32,937: INFO/MainProcess] Received task: celery_app.add[06f0a394-2f48-4113-8dc7-627adae87033]  
[2019-09-30 12:37:32,939: INFO/ForkPoolWorker-1] Task celery_app.add[06f0a394-2f48-4113-8dc7-627adae87033] succeeded in 0.0017761470007826574s: 4
[2019-09-30 12:37:32,940: INFO/MainProcess] Received task: celery_app.add[f17a5b35-78de-4b26-aee5-f8bf530a485d]  
[2019-09-30 12:37:32,941: INFO/ForkPoolWorker-1] Task celery_app.add[f17a5b35-78de-4b26-aee5-f8bf530a485d] succeeded in 0.00044833699939772487s: 6
```

Complimentary to task execution in a chain, tasks can be run parallel as a group.
```python
from celery import group                                                                                                                                                                                   
group_prod = group(prod.s(1, 2), prod.s(2, 3))                                                                                                                                                             
group_prod.delay() 
```
The celery logs should refect something like the following.
```console
[2019-09-30 12:45:08,315: INFO/ForkPoolWorker-1] Task celery_app.prod[0e618246-017a-43d3-ba10-75a3a5cff6f6] succeeded in 0.0016198869998333976s: 2
[2019-09-30 12:45:08,328: INFO/ForkPoolWorker-2] Task celery_app.prod[325de9f0-8cf2-4665-be68-839a2cfe3e8d] succeeded in 0.01336536199960392s: 6
```

One could combine these structures to form a _canvas_.
```python
canvas_add_prod = ( 
    add.s(1, 1) 
    | 
    group(prod.s(2), prod.s(3)) 
)                                                                                                                                                                                                         
canvas_add_prod.delay()
```
Log message
```console
[2019-09-30 12:53:07,313: INFO/MainProcess] Received task: celery_app.add[d6b078b9-2d0c-4bca-b0e1-daa29691f37b]  
[2019-09-30 12:53:07,385: INFO/MainProcess] Received task: celery_app.prod[f3a963b2-732a-4272-ab7a-45fce87a10eb]  
[2019-09-30 12:53:07,387: INFO/MainProcess] Received task: celery_app.prod[7fa2bb70-9155-420a-a5f7-d9116d21f4fd]  
[2019-09-30 12:53:07,387: INFO/ForkPoolWorker-1] Task celery_app.add[d6b078b9-2d0c-4bca-b0e1-daa29691f37b] succeeded in 0.07064567899942631s: 2
[2019-09-30 12:53:07,389: INFO/ForkPoolWorker-1] Task celery_app.prod[7fa2bb70-9155-420a-a5f7-d9116d21f4fd] succeeded in 0.00042736400064313784s: 6
[2019-09-30 12:53:07,391: INFO/ForkPoolWorker-2] Task celery_app.prod[f3a963b2-732a-4272-ab7a-45fce87a10eb] succeeded in 0.003965553998568794s: 4
```
One could also take the advantage of loops to construct the canvas structure, for example,
```python
res = group(add.si(1, ii) for ii in range(4)).delay()
res.get()  # fetches the result, not to be used usually
```
## Countdown and retry
As mentioned earlier, one might want a certain task to start, say, 30s after submission.
This can be easily achieved by the `countdown` keyword.
```python
canvas_add_prod = ( 
    add.s(1, 1) 
    | 
    group(prod.s(2), prod.s(3)) 
)                                                                                                                                                                                                         
canvas_add_prod.apply_async(countdown=30)
```
Note the log message printed upon submission gives an ETA.
```console
[2019-09-30 13:07:31,497: INFO/MainProcess] Received task: celery_app.add[816f4511-a3ce-4262-8ee2-3270df7fa87b]  ETA:[2019-09-30 18:08:01.489036+00:00] 
```
You could also specify options like `eta=tomorrow`  or even set a timezone for the task to run.
See [further documentation](https://docs.celeryproject.org/en/latest/userguide/calling.html#eta-and-countdown).

In case a task throws an error, it can be easily retried. In fact, this is one of the more useful and
convenient feature of `celery`. A relevant use case is when you are trying to download a file but there
is an interruption which results in an `HTTPError`. Let's cook up a case when a task will throw an error.
Modify the `celery_app.py` as follows:
```python
from socket import gaierror
from celery import Celery
from ligo.gracedb.rest import GraceDb

app = Celery('basic_tasks', broker = 'redis://')
app.conf['result_backend'] = app.conf.broker_url

g = GraceDb()

@app.task
def add(x, y):
    return x + y


@app.task
def prod(x, y):
        return x * y


@app.task(autoretry_for=(gaierror,),
          max_retries=5, default_retry_delay=30)
def get_event(graceid):
    return g.event(graceid).json()
```
Here we have added a new task `get_event` which fetches event information from
[GraceDb](https://gracedb.ligo.org/). In the event of absence of internet connection,
the `socket.gaierror` is raised. We could test this using the following sequence
- Turn off the WiFi
- Submit a `get_event` task, which should fail but retried after 30s
```python
from celery_app import get_event
get_event.delay('G298048')
```
- Turn the WiFi back on to see the task succeed.
There are also other options for [retrying](https://docs.celeryproject.org/en/latest/userguide/tasks.html#retrying)
which give control over unexpected scenarios.

## An exercise: Download data products from GraceDb
Write a (or more than one) celery task, with retry options to download and save `bayestar.fits.gz`,
`p_astro.json`, `em_bright.json` for superevents with the following IDs that have the `GCN_PRELIM_SENT`
label.
```console
S190930ad
S190930ac
S190930ab
S190930aa
S190930z
S190930y
S190930x
S190930w
S190930v
S190930u
S190930t
S190930s
```
It is generally a good idea to make the tasks modular so that they can be easily incorporated inside
a canvas. A skeleton solution could be as follows:
```python
@app.task
def get_superevent(superevent_id):
    # download the superevent dictionary

@app.task
def check_label_and_download(event_dictionary, filename):
    # check label and download a filename

@app.task
def save_content(content, filename):
    # save the content in a local file
```
Pseudo code/canvas for one superevent ID could be something like,
```python
(
    get_superevent.s(superevent_id)
    |
    check_label_and_download.s('em_bright.json')
    |
    save_content.s('em_bright_'+superevent_id+'.json')
).delay()
```

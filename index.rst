.. LSC/Virgo Low Latency Bootcamp documentation master file, created by
   sphinx-quickstart on Fri Sep 20 10:18:52 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

LSC/Virgo Low Latency Bootcamp
==============================

Day 1
-----

.. toctree::
   :maxdepth: 1

   bestpractices/index
   ecosystem/index
   coffee
   gracedb/index
   data/data-intro
   astropy/astropy-intro
   healpix/index
   lunch
   tests/index
   officehours

Day 2
-----

.. toctree::
   :maxdepth: 1

   taskqueues/index
   coffee
   webframeworks/index
   lunch
   packaging/index
   officehours

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

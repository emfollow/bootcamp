Office hours
============

.. figure:: http://giphygifs.s3.amazonaws.com/media/MIY4jpusckRmU/giphy.gif
   :width: 600

   This time is available to experiment with the material that we covered
   today, to work independently or in small groups, to get help from the
   presenters, or for discussion.

GraceDB, LVAlert, and GCN (10:30-11:00, Pace)
=============================================

Introduction
------------

As members of this audience know, GraceDB serves as the repository of 
candidate gravitational-wave events. Astrophysical events of importance 
by limited external observing partners are also cataloged in GraceDB. 
The information in GraceDB available for utilization by analysts and 
automated follow-up processes. Permissions exist to control who can 
view and alter GraceDB.

Events in GraceDB-- computational, not astrophysical events-- are 
communicated to users and follow-up processes via Ligo-Virgo Alerts
(LVAlert). This includes new candidate event uploads, candidate updates,
and the like. 

External Links
************** 

These are some links that might be helpful in guiding discussion:

* `GraceDB Server Code Repo <https://git.ligo.org/lscsoft/gracedb>`_ 
* `GraceDB Client Code Repo <https://git.ligo.org/lscsoft/gracedb-client>`_

  - `GraceDB Client Documentation <https://ligo-gracedb.readthedocs.io/en/gracedb-2.4.0/>`_
* `LVAlert Client Code Repo <https://git.ligo.org/lscsoft/lvalert>`_

  - `LVAlert Client Documentation <https://lscsoft.docs.ligo.org/lvalert/>`_
* `LVAlert Status <https://ldas-jobs.ligo.caltech.edu/~alexander.pace/cgi-bin/lvalert_stats>`_
* `LVAlert Account Management <https://lvalert-test.cgca.uwm.edu/>`_

GraceDB
-------

The gravitational-wave candidate event database (GraceDB_) is a service operated 
by the LIGO Scientific Collaboration. It provides a centralized 
location for aggregating and retrieving information about candidate 
gravitational-wave events. GraceDB provides an API for programmatic access, 
and a client package is available for interacting with the API.

The GraceDB service can be divided into five major components:

- **Django Application**: GraceDB is written in Python and is constructed around the Django web framework.

- **Database Backend (MariaDB)**: AWS deployments interface with Amazon Relational Database (ARD) implementation.

- **Backend Webserver (Gunicorn)**: Gunicorn is a lightweight Python webserver which interfaces directly with the Django service via the WSGI protocol. 

- **Frontend Webserver (Apache)**: Used in concert with Gunicorn as an interface with Shibboleth. It is configured as a reverse proxy which gets authentication information from Shibboleth, sets that information in the headers, and then passes it on to Gunicorn.

- **Primary Authentication (Shibboleth)**: software package for managing federated identities and providing a single sign-on (SSO) portal. It uses metadata providers to collect user attributes from an attribute authority and put them into the user’s session. These attributes are then available to the relevant service providers which are accessed by the user.

AWS Deployment
**************

Please refer to `these slides <https://dcc.ligo.org/LIGO-G1900820>`_ for more information on the AWS GraceDB deployment.

.. image:: images/aws.png
   :scale: 30%
   :align: center

The short summary is as follows: 

- Eight containerized instances (two for ``gracedb-test.ligo.org``) of GraceDB (alongsize a ``systemd`` deployment of the ``lvalert-overseer``) in kubernetes cluster.
- Incoming ``http`` traffic routed to GraceDB via two Traefik load-balancer containers
- Existing MySQL/MariaDB interface to `Amazon Relational Database <https://aws.amazon.com/rds/>`_.

GraceDB Philosophy
******************

What follows is a bit of a soapbox. GraceDB is a repository for candidate 
events. Permission to see or create candidates in GraceDB are dictated 
by a matter of policy. Access to candidate events is granted by users or
follow-up policies acting on that policy. 

In short: GraceDB should be as "dumb" as possible. In this context, this 
means that GraceDB should make as few decisions as possible. Arbitrary 
logic to enable arbitrary actions should be handled by outside users 
using the existing API tools, unless a sufficiently compelling case can 
be made to motivate a larger change.

LVAlert
-------

The LIGO-Virgo Alert System (LVAlert) is a notification
service built on the XMPP_ protocol and the pubsub extension.
It provides a basic notification tool which allows multiple producers
and consumers of notifications.

The current and legacy (to be depreciated)  LVAlert clients are powered by
SleekXMPP_ and PyXMPP_, respectively. The server backend is powered by
Openfire_ v4.4.1.


XMPP Nomenclature and Definitions
*********************************

LVAlert is based on the XMPP protocol, and in particular the 
Publish-Subscribe (Pubsub) extension. To better understand the communication
taking place over LVAlert, it's useful to cover some basic definitions. 

.. image:: images/lvalert_xmpp.png
   :scale: 50 %
   :align: center

In LVAlert's implementation of the pubsub extensions, the following definitions
apply, starting from right to left in the above diagram. 

- **LVAlerts**: Plaintext, JSON-formatted strings containing event (computational and astrophysical) information. A description of LVAlert contents can be found `on GraceDB's documentation page <https://gracedb.ligo.org/documentation/lvalert.html#contents-of-lvalerts-sent-by-gracedb>`_.

- **Subscribers**: LVAlert users (like yourselves). Users must have valid LIGO credentials in order to `create an LVAlert account <https://lvalert-test.cgca.uwm.edu/>`_. Once active, all LVAlert users are authorized to receive LVAlerts from **nodes** to which they're subscribed. 

- **Nodes**: or "topics" recieve messages and push them to subscribers. Nodes can be created by any user, however only that user has **publisher** credentials, meaning only that user can push messages to that node, and then to subscribers. As such, there are nodes on LVAlert that exclusively recieve messages from GraceDB. As is stated in `GraceDB's documentation <https://gracedb.ligo.org/documentation/lvalert.html#event-nodes>`_, these node names take specific form based on the group, pipeline, and search. LVAlerts are sent recursively to the ``<group>_<pipeline>`` and the ``<group>_<pipeline>_<search>`` nodes. For instance, an event update from the CBC group, gstlal pipeline, and AllSky search would send an LVAlert to the following nodes:
 - ``cbc_gstlal``
 - ``cbc_gstlal_allsky``

- **Publishers**: Users/services producing messages and pushing to nodes. Likely to be GraceDB. 



Example: My First LVALert Listener
----------------------------------

Our fun exercise for this section is to write our first standalone application to 
interact with the GraceDB and LVAlert ecosystem. A secondary goal of this tutorial is
to get users weaned off of the old ``lvalert_listen`` application before they are 
depreciated in a future release of ``ligo-lvalert``. 

The basic framework of this tool is available in `the ligo-lvalert git repository <https://git.ligo.org/lscsoft/lvalert/blob/master/share/lvalert_listener>`_. To see other examples 
please see:

- `gstlal_inspiral_lvalert_uberplotter <https://git.ligo.org/lscsoft/gstlal/blob/master/gstlal-inspiral/bin/gstlal_inspiral_lvalert_uberplotter>`_ - listens for LVAlerts, downloads files from GraceDB, makes/uploads files to GraceDB. 
- `lvc_subthresold_listener <https://git.ligo.org/rebecca.ewing/swift-sub-threshold-alert/blob/master/lvc_subthreshold_listener>`_ - (author: Rebecca Ewing) listens for subthreshold superevents, forwards event information to external observing partners (``SWIFT``). 

For this basic example, which is a basic demonstration of the API tools, we would like to:

- Use our credentials to initiate an LVAlert and GraceDB client. 
- Query the available nodes on the LVAlert server, and subscribe to nodes.
- Listen for LVAlerts on our specified nodes.
- Write a log message on a "new" event, which will demonstrate:
 - Parsing LVAlert types, ``["alert_type"]``
 - Utilizing the GraceDB API


Step 1: Setup
*************

The first step is to make sure you have the ``ligo-gracedb`` (at least ``v.2.3.0``) and ``ligo-lvalert`` installed in your environment:

:: 

    pip install ligo-gracedb
    pip install ligo-lvalert

There are legacy dependencies for the ``ligo-lvalert`` on ``libxml2`` and ``m2crypto``, so unless you are currently on the LDG, I give the pip installation a 50% chance of success. 

Next, visit the new `LVAlert Account Management page <https://lvalert-test.cgca.uwm.edu>`_ (`WIP <https://git.ligo.org/alexander.pace/lvalert-admin>`_) to create your LVAlert account: 

.. image:: images/lvalert_login.png
   :scale: 30%
   :align: center

Make sure to add the servers and usernames that you created to your ``~/.netrc`` file 
and create the correct permissions: 

:: 

    machine lvalert-playground.cgca.uwm.edu login albert.einstein password iwasright

::

    chmod 600 ~/.netrc

Step 2: Code Parameters
***********************

This is a quick rundown of what's in our cooking-show example. The full code is 
located in the `bootcamp repository <https://git.ligo.org/emfollow/bootcamp/blob/master/gracedb/lvalert_bootcamp_example>`_. 

Necessary imports:

::

    # Import the ligo.lvalert and some other helper libraries.
    from ligo.lvalert import LVAlertClient

    # Import GraceDB:
    from ligo.gracedb.rest import GraceDb
    from ligo.gracedb.exceptions import HTTPError as http_error

Specify your username: 

:: 

    USERNAME = 'albert.einstein'

Specify the LVAlert and GraceDB servers to communicate with. Note that the GraceDB service URL is prepended with ``https://`` and ends with the ``api/``. It will error out if this is not specified correctly. 

:: 
    
    LVALERT_SERVER = 'lvalert-playground.cgca.uwm.edu'
    GRACEDB_SERVER = 'https://gracedb-playground.ligo.org/api/'

Specify the nodes that you want to listen to. The node ``lvalert-playground_monitor_ping``
and similarly on production, ``lvalert_monitor_ping`` get hit with a 1kb LVAlert every
five seconds. For this exercise, we're going to listen for new events on the ``test_gstlal_allsky``
nodes, but will also listen to the ping nodes to make sure the listener is working. 

::

    NODES = ['lvalert-playground_monitor_ping', 'test_gstlal_allsky']

Initiate the two clients:

::

    client = LVAlertClient(server=LVALERT_SERVER,username=USERNAME)
    gdb_client = GraceDb(service_url=GRACEDB_SERVER)


Okay, so this will bgin the main loop. The code will determine if a node is in the
available LVAlert nodes, and then subscribe to the nodes we specified. An ``on_alert`` class 
will be instantiated, and then will listen out for alerts. Keyboard interrupts will disconnect
the LVAlert listener. 

:: 

    try:
        print("Connecting to: %s" % LVALERT_SERVER)
        if client.connect():
            client.process(block=False)
            print("Connected to: %s" % LVALERT_SERVER)

            # Get nodes and then subscribe to the requested nodes:
            lvalert_nodes = client.get_nodes()

            for node in NODES:
                if node in lvalert_nodes:
                    client.subscribe(node)
                    print("Subscribed to node: %s" % node)
            else:
                    print("No node %s on %s" % (node, LVALERT_SERVER))

            # Start the "on_alert"  listener class:
            listener=on_alert(gracedb=gdb_client)

            # Listen for alerts. Note: on the line below, the 'process_alert'
            # method is part of the 'on_alert' class.
            client.listen(listener.process_alert)

            # Loop indefnitely and wait for messages.
            while True: time.sleep(5)

    except (KeyboardInterrupt, SystemExit):
        # Kill the client upon exiting the loop:
        print("Disconnecting from: %s" % LVALERT_SERVER)
        client.abort()

Easy! Now let's take a look at the ``on_alert`` class, and the ``process_alert`` method
that gets called when an alert is received:

::

    class on_alert(object):
        #--- Initialize ---#
        def __init__(self, gracedb):
            # This is where you initalize stuff for the on_alert class.
            # Get creative with passing in options when you initialize it!
            # You can pass around data from method to method with the "self.*"
            # variable.
    
            print("Initialized on_alert class.")
            self.ID = "This computer"
            self.gdb = gracedb
    
        #--- Ingest an alert ---#
        def process_alert(self, node=None, payload=None):
            try:
                print("%s recieved an alert from node %s" % (self.ID, node))
                self.alert_data = json.loads(payload)
    
                # Do something with the LVAlert data:
                self.gid = self.alert_data["uid"]
                self.thistype = self.alert_data["alert_type"]
                print("%s alert came from event ID: %s" % (self.thistype,self.gid))
                if (self.thistype == 'new'):
                   print("---> writing a log message")
                   self.gdb.writeLog(self.gid, "This log message is written by my listener.")
    
            except Exception as e: print(e)


Step 3: Download and run
************************

I (Alex) will run the program and upload test events to demonstrate the program working.

GCN
---

I'm going to defer the GCN discussion to the larger group, or at least to a member of 
the audience with greater expertise. That being said, I'm going to outline the workflow
as far as I understand it: 

* Data products (candidate event parameters) live on GraceDB. External processes or people determine that the greater community should be alerted about the event. 

* A third party (GWCelery?) requests a ``VOEvent`` from GraceDB. A VOEvent is an XML-formatted, machine-readable string. The VOEvent generator is based on ``voeventlib`` that is not maintained by GraceDB, but (for legacy reasons) lives on GraceDB.

* GWCelery requests the VOEvent from GraceDB, and then forwards it to a machine living at NASA Goddard that ingests the machine-readable file, parses it, and then generates a human-readable notice.

.. _GraceDB: https://gracedb.ligo.org/
.. _XMPP: https://xmpp.org/
.. _SleekXMPP: http://sleekxmpp.com
.. _PyXMPP: http://pyxmpp.jajcus.net/
.. _Openfire: https://www.igniterealtime.org/projects/openfire/

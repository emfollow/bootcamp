from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
@app.route('/hello')
@app.route('/hello/<name>')
def hello(name="y'all"):
    return render_template('hello2.html', name=name, title='hello')

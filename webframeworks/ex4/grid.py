from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
def image_grid():
    return render_template('content.html')

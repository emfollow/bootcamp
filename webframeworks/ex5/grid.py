from flask import Flask, render_template
app = Flask(__name__)

@app.route('/grid/<int:num_rows>/<int:num_cols>')
def image_grid(num_rows, num_cols):
    return render_template(
        'content.html',
        num_rows=num_rows,
        num_cols=num_cols,
    )

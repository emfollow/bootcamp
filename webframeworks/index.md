# Web Frameworks (11:00-12:30, Godwin)

## Introduction

A web framework provides a toolkit to easily build and deploy web applications. These include websites, web services, and REST APIs. They can be used to build both static and dynamic websites. Using a web framework exposes common web functionality and provides guidelines to structure a project for easier maintainability.

### Comparison of Web Frameworks

For the purposes of this tutorial, we will be focusing on python-based web frameworks. There are plenty of choices to choose from, depending on your use case and flexibility needs. Many web frameworks interface with a templating engine to render HTML, JSON and other common output formats. They also provide a clean way of providing URL routing, input form handling and validation, and other operations. Some frameworks tend to provide a minimal set of features out-of-the-box, having the freedom to pick and choose extensions to extend functionality as you need it. Others provide a "batteries-included" approach, which ship bundled with many of these features already. Those frameworks take a more opinionated approach, choosing specific technologies that work well together.

Flask, Bottle and Falcon all take a minimalist approach, leaving the choice of tooling for application architecture to the developer, such as authentication and database administration. On the other end of the spectrum, Django chooses all these tools for you already and provides an ecosystem that makes it easy to interact with all these various tools to create enterprise-level web application. Flask gives you templating, routing and forms in the main package, but leave form validation and authentication as extensions you'll need to set up separately, or optionally write your own extension that has the features you want. This makes it easier to set up small scale web applications, at the cost of making decisions when you need to set up authentication, for example.

In this tutorial, we will be focusing on Flask.

### HTTP and Web Applications

Web browsers display web pages by making a request to a web server using the HTTP protocol, returning HTML-formatted content. The HTTP protocol is based on a request-response model. Clients such as your browser makes an HTTP request to a web server hosting a web application, and the web application responds to the request with the data requested. Messages within the HTTP protocol have an associated method with that request. The two most common HTTP methods are `GET` and `POST`. The `GET` method requests, or GETs, data from the web server. In this case, the web application only needs to respond to the request with a response code and the message. A `POST` method allows clients to send, or POST, requests to the web application. This results in an action taking place in the web application. For example, a submitting credentials on a sign in page is handled by a `POST` request.

In addition to data requests, each request also returns an HTTP response code which indicate the status of a given request. Each response is a 3-digit code. They can be split up into categories, depending on the status. 2xx codes indicate a success, 3xx codes indicate a redirection, 4xx indicate a client error, and 5xx indicate a server error.

### Client-side vs Server-side Frameworks

We will be focusing on server-side web frameworks in this tutorial. For server-side (or back-end) frameworks, we focus on handling and processing requests on the web server. We define URLs and what type of HTTP methods are allowed with those associated URLs. All content rendering is done by the web server, and no processing is done by the client. Python web frameworks are focused on server-side operation. Alternatively, some frameworks focus on client-side rendering and processing. These are almost exclusively javascript-based. For example, web-based dashboards that periodically poll for data and display updated information are commonly done leveraging a front-end framework to store state.

## Flask

Flask is a python-based web 'micro-framework', meaning that it provides a minimal set of features to build a web application, and relies on a series of extensions to provide optional services, like form validation, database access, etc. This allows developers creating a web application to pick and choose what features they need for their particular use-case.

Flask's documentation is extensive and is a great resource for picking up various aspects of web application development. It can be found here: <https://flask.palletsprojects.com/en/1.1.x/>

### A simple Flask application

```
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello():
    return 'Howdy y'all!'
```

We can run this application by telling Flask where to find the Flask application (`FLASK_APP`) and running it with `flask run`, as follows:

```
$ export FLASK_APP=myapp.py
$ flask run
```

Doing this will spin up a development WSGI server that Flask ships for testing purposes. This won't scale in production but is more than suitable for the examples provided in this tutorial.

Let's try this out by running the provided example in `webframeworks/ex1/hello.py` and navigating to http://localhost:5000. I have provided a `requirements.txt` in the `webframeworks` folder that you can use to install Flask + dependencies in a python virtualenv.

### The `Flask` application instance

Creating a web application in Flask requires an application instance, which allows the web server to pass HTTP requests to the `Flask` object to handle such requests.

```
from flask import Flask
app = Flask(__name__)
```

Instantiating a `Flask` instance requires a single argument, the name of the main module or package of the application. For single module applications, `__name__` is the correct value to use. If you are creating a package, you can use the name of the package instead, but `__name__` will still work.

### Routing

A web server that serves a web application handles client-side requests, which gets sent to the Flask application instance. The application needs to know what content to serve based on the URL that is required, so it maps URLs to python functions. This mapping between URLs and functions that serves content is called a route.

```
@app.route('/')
def hello():
    return 'Howdy y'all!'
```

The `route()` decorator is used within Flask to bind a python function to a URL. In this case, the root URL is bound to the function `hello()` which returns back a friendly message. A decorator is a function that takes another function and extends its behavior without modifying it. 

Flask allows you to specify dynamic routes as well, which allow you to dynamically create content by parameterizing URLs.

```
@app.route('/hello/<name>')
def hello(name):
    return '<h2>Howdy, {}!</h2>'.format(name)
```

In addition to accepting variables, we can enforce argument types allowed in dynamic routes:

```
@app.route('/add/<int:a>/<int:b>')
def add(a, b):
    return '{} + {} = {}'.format(a, b, a + b)
```

We can also allow for default values in dynamic routes and attach multiple routes to the same function.

```
@app.route('/')
@app.route('/hello')
@app.route('/hello/<name>')
def hello(name="y'all"):
    return '<h2>Howdy, {}!<h2>'.format(name)
```

Let's try these out by running the provided example in `webframeworks/ex2/routes.py`.

### The `request` object

Flask also exposes a `request` object to handle information that the client included in the HTTP request. These include form fields and arguments passed in the query string of the URL. Query strings in the URL take the form `http://mydomain.org/route?arg1=value1&arg2=value2`. This can complement the routes defined by the web application. For example, say you have a route that returns the 'last event' recorded, and the default format returned is a gps time. However, some clients would rather return a datetime-formatted response instead. Instead of having two routes, you could instead have a route with a `format` arg and if that's set to `datetime`, return a different format instead. Here's an example:

```
from flask import Flask, request
app = Flask(__name__)

@app.route('/event')
def handle_event():
    query = request.args
    if 'format' in query and query['format'] == 'datetime':
        # return datetime-formatted event
    else:
        # return gpstime-formatted event
```

### Templates

In the examples seen so far, the responses returned by the web app have been minimal in terms of HTML content. We may want to display a webpage that contains both static and dynamic content based on the client's request. For example, say we have a route that displays a HTML-formatted event table and a web form that allows you to filter events by time ranges and event type. This may involve a call to a database that returns a series of rows, and populate a table based on those results. There is logic to query a database and generate the content, as well as logic to present the data in a suitable form. Templates allow us to separate out the presentation logic to help with overall maintainability.

A template is a file that contains both static data and placeholders for the dynamic data, and is rendered to generate a properly formatted response. Flask leverages a templating engine, `Jinja2`, to render the templates. It's fairly common to use HTML or javascript-based templates for web applications.

Let's modify the hello route to use a template instead. By setting the argument in the Flask app to `__name__`, Flask expects templates to live in the `/templates` directory.

`templates/hello1.html`:
```
<h2>Howdy, {{ name }}!<h2>
```

We can define variables that get filled in when the template is being rendered by adding brackets surrounding the variable, i.e. `{{ name }}`.

`hello1.py`:
```
from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
@app.route('/hello')
@app.route('/hello/<name>')
def hello(name="y'all"):
    return render_template('hello1.html', name=name)
```

Try this out by running the provided example in `webframeworks/ex3/hello1.py`.

Now let's add some minimal HTML to show how we can use base templates to modularize and reuse recurring structures. A base template defines blocks that can then be filled out by templates that extend this base template. In this case, we'll define a base template that just has the minimal HTML necessary to create a webpage.

`templates/base.html`:
```
<html>
<head>
    <title>{{ title }}</title>
</head>
<body>
    {% block body %}
    {% endblock %}
</body>
</html>
```

Note that I also added a title variable that can be filled out. We can define blocks and other control structures by surrounding content with `{% ... %}` that will get filled out by jinja2. To define blocks, we use the `{% block <name> %}` and `{% endblock %}` tags. For each control structure, jinja2 needs to know where the control structure ends. We will see more examples of this in the following sections. We'll also create a template that uses this and adds some minimal HTML in the body.

`templates/hello2.html`:
```
{% extends 'base.html' %}
{% block body %}
<h2>Howdy, {{ name }}!<h2>
{% endblock %}
```

Here, we define the same `body` block that gets filled into the base template. The `extends` directive specifies that this template derives from `base.html`.

`hello2.py`:
```
from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
@app.route('/hello')
@app.route('/hello/<name>')
def hello(name="y'all"):
    return render_template('hello2.html', name=name, title='hello')
```

Here we added the title variable, and we see that we don't need to distinguish which variables get passed to the base template vs. the derived template. Try this out by running the provided example in `webframeworks/ex3/hello2.py`.

### Interlude: Bootstrap CSS

Now that we can render HTML templates, let's briefly cover a front-end framework, Bootstrap. This will allow us to start filling our webpages with richer and more visually appealing content. Bootstrap is a minimal HTML, CSS, and JS framework that allows us to build responsive webpages with minimal effort. It contains a grid system to allow you to partition your content in various ways, as well as a variety of general-purpose components such as navbars and buttons. GWCelery, the LIGO Summary Pages and ligo-scald all leverage Bootstrap to generate HTML-formatted content.

Let's generate a base template that imports the bootstrap libraries and contains the HTML head, and another template that extends off the base template and contains the HTML body with a 2x2 grid of randomly generated images.

`templates/base.html`:
```
<html>
<head>
    <!-- css imports -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <!-- js imports -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"</script>
</head>
<body>
    {% block body %}
    {% endblock %}
</body>
</html>
```

The `base.html` template contains the HTML header, Bootstrap css, js and required dependencies. It also includes a skeleton for the rest of the content to go in.

`templates/content.html`:
```
{% extends 'base.html' %}
{% block body %}
<div class="container mt-3">
<h3>This is an image grid</h3>
  <div class="row my-3">
    <div class="col-6">
      <img class="img-fluid img-thumbnail" src="https://picsum.photos/seed/one/800/600" alt="">
    </div>
    <div class="col-6">
      <img class="img-fluid img-thumbnail" src="https://picsum.photos/seed/two/800/600" alt="">
    </div>
  </div>
  <div class="row mb-3">
    <div class="col-6">
      <img class="img-fluid img-thumbnail" src="https://picsum.photos/seed/three/800/600" alt="">
    </div>
    <div class="col-6">
      <img class="img-fluid img-thumbnail" src="https://picsum.photos/seed/four/800/600" alt="">
    </div>
  </div>
</div>
{% endblock %}
```

The `content.html` extends off this base template and defines a 2x2 image grid with a header. We can create a grid by making rows. Using `<div class="row">` generates rows and putting `<div class="col">` adds columns for these rows. The grid system is divvied up into 12 units, so specifying 6 units for each column and two rows will generate a 2x2 grid. The images are generated with `<image>` tags with the img-fluid class, which allows the image to fit the parent element and makes it responsive. The img-thumbnail class adds a small border around the image. A container (`<div class="container">`) defines a single padded section to wrap the content around.

`grid.py`:
```
from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
def image_grid():
    return render_template('content.html')
```

The Flask app, `grid.py`, is very barebones and there to serve the content that is rendered by the template. Try this out by running the provided example in `webframeworks/ex4/grid.py`.

### More templates

We've seen some examples of control structures in templates already, by extending templates and filling out content blocks. In addition, we can embed conditional statements and for loops into the templates themselves. Let's extend the grid example to be able to resize the grid dynamically using control structures.

`templates/content.html`:
```
{% extends 'base.html' %}
{% block body %}
<div class="container mt-3">
<h3>This is an image grid</h3>
  {% for i in range(num_rows) %}
    <div class="row my-3">
      {% for j in range(num_cols) %}
        <div class="col">
          <img class="img-fluid img-thumbnail" src="https://picsum.photos/seed/{{ j + 1 + (i * num_cols) }}/800/600" alt="">
        </div>
      {% endfor %}
    </div>
  {% endfor %}
</div>
{% endblock %}
```

In addition to being able to add for loops in templates, we can also call python functions inside of the directives, such as `range()`. We can also evaluate python expressions, as in `{{ j + 1 + (i * num_cols) }}`, which generates a unique image for every image in the grid. Any function defined in a jinja directive is exposed for use inside of the blocks, like `i` and `j`.

`grid.py`:
```
from flask import Flask, render_template
app = Flask(__name__)

@app.route('/grid/<int:num_rows>/<int:num_cols>')
def image_grid(num_rows, num_cols):
    return render_template(
        'content.html',
        num_rows=num_rows,
        num_cols=num_cols,
    )
```

Here, we also extend the route to take in the number of rows, `num_rows`, and the number of columns, `num_cols`. We also enforce the variable type to be an integer, which guarantees that the behavior of generating arbitrary grids has sensible behavior.

Try playing around with image grid size by running the provided example in `webframeworks/ex5/grid.py`.

Two other nice features of jinja templates are adding conditional statements and macros. Adding conditionals allows you to generate different content based on a set of conditions. For example:

```
...
<head>
  {% if title %}
    <title>{{ title }}</title>
  {% else %}
    <title>hello!</title>
  <% endif %}
</head>
...
```

Macros are similar to python functions for templates, and can be imported for use in other templates. For example, we can add a macro that wraps text in header tags, which can then be used anywhere this macro is imported:

`utils.html`:
```
{% macro add_header(header) %}
  <h1>{{ header }}<h1>
{% endmacro %}
```

`main.html`:
```
{% import 'utils.html' as utils %}
{{ utils.add_header(header) }}
```

### JSON-formatted responses

So far, we've focused on HTML-based responses, but Flask also has first-class support for returning JSON-formatted responses which can be of great use if you're designing a RESTful web service. Any response that needs to be JSON-formatted can be generated by using Flask's `jsonify` function. For example:

```
from flask import Flask, jsonify
app = Flask(__name__)

@app.route('/api/health')
def health_check():
    return jsonify({'status': 'OK'})
```

In this particular case, `jsonify` wasn't strictly needed since any routes that return simple dictionaries are automatically converted to JSON. In general, this won't be the case and using `jsonify` simplifies the process of JSON serialization.

Another example which returns back JSON which necessitates the use of `jsonify`:

```
@app.route('/api/data')
def squared():
    return jsonify({'x': [1, 2, 3], 'y': [1, 4, 9])
```

### Project Structure

As your web application project grows, we may want to factor out different app components into various pieces, as follows:

```
requirements.txt
app/
    __init__.py
    views.py
    models.py
    static/
    templates/
```

Inside the package that contains the web application, `app`, all the route definitions are contained in `views.py`, the templates in `templates/` and the static files (images, js, css) in `static/`. `__init__.py` contains the Flask application instance, which is then hooked up to the various routes in `views.py`. If needed, we can also store database models in `models.py` that are used in conjunction with the `SQLAlchemy` Flask extension. 

### Flask extensions

It was mentioned before that Flask contains a minimal framework to build a web application, but there are many extensions you can add that focus on various features that may be useful depending on your use case. I'll highlight a few popular extensions below:

* Flask-SQLAlchemy:

   If you're looking to make heavy use of relational databases, SQLAlchemy is a popular python package that ships with an ORM (Object Relational Model) and several utilities to easily provide access to many different database backends. The Flask-SQLAlchemy extension adds several database utilities can can be accessed easily within the Flask framework.

* Flask-WTF:

   This plugin allows a simplied way of generating web forms with form validation.

* Flask-Caching:

   This plugin adds everything to do with caching for Flask.

* Flask-Logins:

   This plugin simplify the use of adding login support for Flask, adding various features for user session management.

There are many extensions available on PyPI: <https://pypi.org/search/?c=Framework+%3A%3A+Flask>

## Hands-on Exercise

We will be using the grid example as a baseline, and add a few features.

1. Add a default route `/` that defaults to a 3x3 grid.
2. Add a route that takes in a single parameter, N, which generates an NxN grid.
3. Add a button that extends the grid by one row and column.
  * Hint 1: You can find the current URL by calling `request.path`.
  * Hint 2: Docs for a Bootstrap-style button: <https://getbootstrap.com/docs/4.0/components/buttons/>

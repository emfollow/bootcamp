from flask import Flask
app = Flask(__name__)

@app.route('/')
@app.route('/hello')
@app.route('/hello/<name>')
def hello(name="y'all"):
    return 'Howdy, {}!'.format(name)

@app.route('/add/<int:a>/<int:b>')
def add(a, b):
    return '{} + {} = {}'.format(a, b, a + b)
